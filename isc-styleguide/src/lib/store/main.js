export const storeTest = {
    state: () => ({
        count: 0
    }),
    mutations: {
        aumentar(state) {
            state.count++
        }
    }
}