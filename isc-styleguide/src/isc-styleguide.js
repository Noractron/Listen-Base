import "./set-public-path";
import "./app.scss";
import Vue from "vue";
import Vuex from "vuex";
import {storeTest} from "./lib/store/main"
// Anything exported from this file is importable by other in-browser modules.
Vue.use(Vuex);
export { default as querys } from "./lib/graphql/querys";

export const store = new Vuex.Store({
    modules: {
        storeTest
    }
})