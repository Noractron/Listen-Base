export function prefix(location, ...prefixes) {
    return prefixes.some(
      prefix => location.href.indexOf(`${location.origin}/${prefix}`) !== -1
    );
  }
  
  export function sidebarNav() {
    // The nav is always active
    return true;
  }

  export function dashboard(location) {
    return prefix(location, 'dashboard');
  }

  export function overview(location) {
    return prefix(location, 'overview');
  }

  export function cluster_analysis(location) {
    return prefix(location, 'cluster-analysis');
  }

  export function geo_listening(location) {
    return prefix(location, 'geo-listening');
  }