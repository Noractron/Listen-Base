import { registerApplication, start } from "single-spa";
import * as isActive from "./activity-functions";
import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

registerApplication(
  '@isc/sidebar-nav',
  () => System.import('@isc/sidebar-nav'),
  isActive.sidebarNav,
  { domElement: document.getElementById('sidebar-nav') }
)

registerApplication(
  '@isc/dashboard',
  () => System.import('@isc/dashboard'),
  isActive.dashboard,
  { domElement: document.getElementById('dashboard') }
)

registerApplication(
  '@isc/overview',
  () => System.import('@isc/overview'),
  isActive.overview,
  { domElement: document.getElementById('overview') }
)

registerApplication(
  '@isc/cluster-analysis',
  () => System.import('@isc/cluster-analysis'),
  isActive.cluster_analysis,
  { domElement: document.getElementById('cluster-analysis') }
)

registerApplication(
  '@isc/geo-listening',
  () => System.import('@isc/geo-listening'),
  isActive.geo_listening,
  { domElement: document.getElementById('geo-listening') }
)


start();
