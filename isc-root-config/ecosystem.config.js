module.exports = {
  apps: [
    {
      name: "root-config",
      script: "./isc-root-config/node_modules/webpack/bin/webpack.js",
      args: "serve --port 9000 --env isLocal",
      cwd: "./isc-root-config",
      watch: false,
    },
    {
      name: "styleguide",
      script: "./isc-styleguide/node_modules/webpack/bin/webpack.js",
      args: "serve",
      cwd: "./isc-styleguide",
      watch: false,
    },
    {
      name: "sidebar-nav",
      script:
        "./isc-sidebar-nav/node_modules/@vue/cli-service/bin/vue-cli-service.js",
      args: "serve --port 8081",
      cwd: "./isc-sidebar-nav",
      watch: false,
    },
    {
      name: "geo-listening",
      script:
        "./isc-geoListening/node_modules/@vue/cli-service/bin/vue-cli-service.js",
      //isc-benchmark\node_modules\@vue\cli-service\bin\vue-cli-service.js
      args: "serve --port 8084",
      cwd: "./isc-geoListening",
      watch: false,
    },
    {
      name: "overview",
      script:
        "./isc-overview/node_modules/@vue/cli-service/bin/vue-cli-service.js",
      args: "serve --port 8082",
      cwd: "./isc-overview",
      watch: false,
    },
    {
      name: "cluster-analysis",
      script:
        "./isc-clusterAnalysis/node_modules/@vue/cli-service/bin/vue-cli-service.js",
      args: "serve --port 8083",
      cwd: "./isc-clusterAnalysis",
      watch: false,
    },
    {
      name: "dashboard",
      script:
        "./isc-dashboard/node_modules/@vue/cli-service/bin/vue-cli-service.js",
      args: "serve --port 8080",
      cwd: "./isc-dashboard",
      watch: false,
    },
  ],
};
