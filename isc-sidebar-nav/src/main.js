import './set-public-path'
import Vue from 'vue';
import App from './App.vue';
import singleSpaVue from 'single-spa-vue';
import router from './router.js';
import { store } from '@isc/styleguide'
Vue.config.productionTip = false;


const vueLifecycles = singleSpaVue({
  Vue,
  appOptions: {
    render: (h) => h(App),
    router,
    store
  },
});

export const bootstrap = vueLifecycles.bootstrap;
export const mount = vueLifecycles.mount;
export const unmount = vueLifecycles.unmount;
